## @dylanASIX M06 2022

### Crear una imatge nova a partir del Dockerfile
```
$ docker build -t dvargasg22/ldap22:practica .
```
### Executar docker en Mode Detach
```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d dvargasg22/ldap22:practica
```
### Executar el PHPLDAPADMIN
```
$ docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d edtasixm06/ldap22:phpldapadmin
```
