## @dylanASIX M06 2022

### Crear una imatge nova a partir del Dockerfile
```
$ docker build -t dvargasg22/ldap22:group .
```
### Executar docker en Mode Detach
```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d dvargasg22/ldap22:group
```
### Executar el PHPLDAPADMIN
```
$ docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d edtasixm06/ldap22:phpldapadmin
```

### dvargasg22/ldap22:group Servidor LDAP amb la base de dades edt.org S'ha fet el següent:
* Modificar el fitxer edt-org.ldif per afegir una ou grups.
* S'han definit els següents grups: alumnes(600), professors(601), 1asix(610), 2asix(611), sudoers(27), 1wiam(612), 2wiam(613), 1hiaw(614). 
