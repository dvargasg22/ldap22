## @dylanASIX M06 2021-2022

### Crear una imatge nova a partir del Dockerfile
```
$ docker build -t dvargasg22/ldap22:editat .
```
### Executar docker en Mode Detach
```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d dvargasg22/ldap22:editat
```
